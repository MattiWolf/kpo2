<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Бытовая химия оптом</title>
    </head>
    <body>
        <header>
            <a href="/"><img alt="Логотип" id="top-image" src="#"/></a>
            <div id="user-panel">
                <a href="#"><img alt="Иконка" scr=""/></a>
                <a href="javascript:void(0);">[Панель для юзера]</a>
            </div>
        </header>
        <div id="main">
            <aside class="leftAside">
                <h2>Товары</h2>
                <ul>
                    <li><a href="#">Моющие средства</a></li>
                    <li><a href="#">Средства для чистки</a></li>
                    <li><a href="#">Дезинфекционные средства</a></li>
                    <li><a href="#">Репелленты</a></li>
                    
                </ul>
            </aside>
            <section>
                <article>
                    <h1>Хиты продаж</h1>
                    <div class="text-article">
                        Чистящий порошок Комет Лимон в банке 475г
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Поставщик: <a href="#">Волкова</a></span>
                        <span class="read"><a href="javascript:void(0);">Заказать...</a></span>
                        <span class="date-article">Цена: 38 р</span>
                    </div>
                </article>
                <article>
                    <h1>Хиты продаж</h1>
                    <div class="text-article">
                        Стиральный порошок Ушастый Нянь 4,5 кг
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Поставщик: <a href="#">Волкова</a></span>
                        <span class="read"><a href="javascript:void(0);">Заказать...</a></span>
                        <span class="date-article">Цена: 448 р</span>
                        
                    </div>
                </article>
            </section>
        </div>
        <footer>
            <div>
                <span>ТиСКПО</span>
                <span><a target="_blanc" href="http://ofiskom.com/bytovaya-himiya">Реклама</a></span>
            </div>
        </footer>
    </body>
</html>
